#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <time.h>

#define STICKS_AND_PHILS 5
#define NUM_OF_TIMES 100

HANDLE sticks[STICKS_AND_PHILS];

void createMultiProcess();
void multiProcess(char* index);

int main(int argc, char* argv[])
{
	if (argc > 1)
	{
		multiProcess(argv[1]);
	}
	else
	{
		createMultiProcess();
		system("pause");
	}
	

	return 0;
}

void createMultiProcess()
{

	PROCESS_INFORMATION pi[5] = { NULL };
	STARTUPINFO si[5] = { NULL };

	for (int i = 0; i < STICKS_AND_PHILS; i++)	//Initializing the array of the sticks(Critical sections.)
	{
		std::string name = "stick" + std::to_string(i);
		const char* mtx = name.c_str();
		sticks[i] = CreateMutexA(NULL, false, (LPCSTR)mtx);
	}

	printf("Running...\n");
	clock_t beginTime = clock();

	for (int i = 0; i < STICKS_AND_PHILS; i++) //Initializing the array of the threads.
	{
		std::string str = "B.exe " + std::to_string(i);
		std::cout << str;
		const char* name = str.c_str();
		CreateProcess(NULL,
			(LPSTR)name,
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			NULL,
			&si[i],
			&pi[i]);
	}

	WaitForSingleObject(pi[0].hProcess, INFINITE);
	WaitForSingleObject(pi[1].hProcess, INFINITE);
	WaitForSingleObject(pi[2].hProcess, INFINITE);
	WaitForSingleObject(pi[3].hProcess, INFINITE);
	WaitForSingleObject(pi[4].hProcess, INFINITE);

	beginTime = clock() - beginTime;
	printf("The time is: %f\n", (float)beginTime / CLOCKS_PER_SEC);
}

void multiProcess(char* index)
{
	int leftStick = index[0] - '0';
	int rightStick = (leftStick + 1) % 5;

	HANDLE lStick = CreateMutex(NULL, NULL, (LPCTSTR)("stick" + leftStick));
	HANDLE rStick = CreateMutex(NULL, NULL, (LPCTSTR)("stick" + rightStick));

	for (int i = 0; i < NUM_OF_TIMES; i++)
	{
		while (WaitForSingleObject(lStick, INFINITE) != WAIT_OBJECT_0) {}
		while (WaitForSingleObject(rStick, 0) != WAIT_OBJECT_0)
		{
			ReleaseMutex(lStick);
			while (WaitForSingleObject(lStick, INFINITE) != WAIT_OBJECT_0) {}
		}

		printf("eating... ->     %d\n", leftStick);	//Printing the eating state. WARNING -> takes a lot of time for 1Mill times.

		ReleaseMutex(lStick);
		ReleaseMutex(rStick);
	}
	printf("Finished %d\n", leftStick);
}