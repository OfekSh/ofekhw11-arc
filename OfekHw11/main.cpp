#include <windows.h>
#include <stdio.h>
#include <time.h>
#define STICKS_AND_PHILS 5
#define NUM_OF_TIMES 100

DWORD WINAPI wantToEat(LPVOID param);
CRITICAL_SECTION sticks[STICKS_AND_PHILS];

int main()
{
	HANDLE WINAPI phil[STICKS_AND_PHILS];
	int indexes[STICKS_AND_PHILS] = { 0, 1, 2, 3, 4 };

	for (int i = 0; i < STICKS_AND_PHILS; i++)	//Initializing the array of the sticks(Critical sections.)
	{
		InitializeCriticalSection(&(sticks[i]));
	}
	clock_t beginTime = clock();
	for (int i = 0; i < STICKS_AND_PHILS; i++) //Initializing the array of the threads.
	{
		phil[i] = CreateThread(
			NULL,
			0,
			wantToEat,
			&indexes[i],
			0,
			NULL);
	}

	WaitForSingleObject(phil[0], INFINITE);
	WaitForSingleObject(phil[1], INFINITE);
	WaitForSingleObject(phil[2], INFINITE);
	WaitForSingleObject(phil[3], INFINITE);
	WaitForSingleObject(phil[4], INFINITE);

	beginTime = clock() - beginTime;
	printf("The time is: %f\n", (float)beginTime / CLOCKS_PER_SEC);
	system("pause");
	return 0;
}


DWORD WINAPI wantToEat(LPVOID param)
{
	int leftStick = *(LPINT)param;
	int rightStick = (leftStick + 1) % 5;

	for (int i = 0; i < NUM_OF_TIMES; i++)
	{
		EnterCriticalSection(&(sticks[leftStick]));
		while (!TryEnterCriticalSection(&sticks[rightStick]))
		{
			LeaveCriticalSection(&(sticks[leftStick]));
			EnterCriticalSection(&(sticks[leftStick]));
		}
		
		printf("Eating.. :    ->   %d\n", leftStick + 1);		//Activate to eat. WARNING: Takes a LOT of time to finish 1Mil times.

		LeaveCriticalSection(&(sticks[leftStick]));
		LeaveCriticalSection(&(sticks[rightStick]));
	}
	printf("Finished %d\n", leftStick);
	return 0;
}